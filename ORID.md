# **What did we learn today? What activites did you do? What scenes have impressed you**
I have been practicing TDD all day today, and in the process of practice, I have a deeper understanding of the process of TDD and a better understanding of the TDD model.
# **Pleas use one word to express your feelings about today's class.**
useful
# **What do you think about this? What was the most meaningful aspect of this activity?**
Today's content is very substantial, and among them I think the most beneficial module should be TDD, it brings a new programming idea.
# **Where do you most want to apply what you have learned today? What changes will you make?**
I spend most of my time practicing the TDD model today, and in the process of practice, I have a deeper understanding of the TDD model and changed my traditional development mode.


https://gitlab.com/Hilton7/mars-rover-starter