package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SuperSmartParkingBoyTest {
    @Test
    void should_return_ticket_when_park_given_super_smart_parking_boy_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingBoy parkingBoy = new SuperSmartParkingBoy(parkingLot);
        Car car = new Car("a");
        //when
        Ticket ticket = parkingBoy.park(car);
        //then
        Assertions.assertEquals(car, ticket.getCar());
    }


    @Test
    void should_return_car_when_fetch_given_super_smart_parking_boy_and_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingBoy parkingBoy = new SuperSmartParkingBoy(parkingLot);
        Car car = new Car("a");
        //when
        parkingBoy.park(new Car("b"));
        Ticket ticket = parkingBoy.park(car);
        Car fetchCar = parkingBoy.fetch(ticket);
        //then
        Assertions.assertEquals(car, fetchCar);
    }

    @Test
    void should_throw_UnrecognizedTicketException_when_fetch_given_super_smart_parking_boy_and_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingBoy parkingBoy = new SuperSmartParkingBoy(parkingLot);
        Car car = new Car("a");
        //then
        UnrecognizedTicketException exceptionMessage = Assertions.assertThrows(UnrecognizedTicketException.class, () -> {
            //given
            Ticket ticket = parkingBoy.park(car);
            Car fetchCar = parkingBoy.fetch(new Ticket());
        });
        Assertions.assertEquals(exceptionMessage.getMessage(), "Unrecognized parking ticket");
    }

    @Test
    void should_throw_UnrecognizedTicketException_when_fetch_given_super_smart_parking_boy_and_repeat_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingBoy parkingBoy = new SmartParkingBoy(parkingLot);
        Car car = new Car("a");
        //then
        UnrecognizedTicketException exceptionMessage = Assertions.assertThrows(UnrecognizedTicketException.class, () -> {
            //when
            Ticket ticket = parkingBoy.park(car);
            Car fetchCar = parkingBoy.fetch(ticket);
            fetchCar = parkingBoy.fetch(ticket);
        });
        Assertions.assertEquals(exceptionMessage.getMessage(), "Unrecognized parking ticket");
    }

    @Test
    void should_throw_WithoutAnyPositionException_when_park_given_full_super_mart_parking_boy_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingBoy parkingBoy = new SuperSmartParkingBoy(parkingLot);
        Car car = new Car("a");
        parkingBoy.park(car);
        //then
        WithoutAnyPositionException exceptionMessage = Assertions.assertThrows(WithoutAnyPositionException.class, () -> {
            //when
            parkingBoy.park(new Car("b"));
        });
        Assertions.assertEquals(exceptionMessage.getMessage(), "No available position");
    }


    @Test
    void should_throw_WithoutAnyPositionException_when_park_given_super_smart_parking_boy_without_position_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(0);
        ParkingBoy parkingBoy = new SmartParkingBoy(parkingLot);
        Car car = new Car("a");
        //then
        WithoutAnyPositionException exceptionMessage = Assertions.assertThrows(WithoutAnyPositionException.class, () -> {
            //when
            parkingBoy.park(car);
        });
        Assertions.assertEquals(exceptionMessage.getMessage(), "No available position");
    }

    @Test
    void should_return_right_car_when_park_given_super_smart_parking_boy_with_two_parking_lots_and_car_and_two_tickets() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new SuperSmartParkingBoy(parkingLot1, parkingLot2);
        Car car1 = new Car("a");
        Car car2 = new Car("b");
        //when
        Ticket ticket1 = parkingBoy.park(car1);
        Ticket ticket2 = parkingBoy.park(car2);
        Car fetchCar1 = parkingBoy.fetch(ticket1);
        Car fetchCar2 = parkingBoy.fetch(ticket2);
        //then
        Assertions.assertEquals(car1, fetchCar1);
        Assertions.assertEquals(car2, fetchCar2);
    }

    @Test
    void should_throw_UnrecognizedException_when_park_given_super_smart_parking_boy_with_two_parking_lots_and_car_and_unrecognized_tickets() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new SuperSmartParkingBoy(parkingLot1, parkingLot2);
        Car car1 = new Car("a");
        //when
        parkingBoy.park(car1);
        //then
        UnrecognizedTicketException exceptionMessage = Assertions.assertThrows(UnrecognizedTicketException.class, () -> {
            parkingBoy.fetch(new Ticket());
        });
        Assertions.assertEquals(exceptionMessage.getMessage(), "Unrecognized parking ticket");
    }

    @Test
    void should_throw_UnrecognizedException_when_park_given_super_smart_parking_boy_with_two_parking_lots_and_car_and_repeat_tickets() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new SuperSmartParkingBoy(parkingLot1, parkingLot2);
        Car car1 = new Car("a");
        //when
        Ticket ticket = parkingBoy.park(car1);
        parkingBoy.fetch(ticket);
        //then
        UnrecognizedTicketException exceptionMessage = Assertions.assertThrows(UnrecognizedTicketException.class, () -> {
            parkingBoy.fetch(ticket);
        });
        Assertions.assertEquals(exceptionMessage.getMessage(), "Unrecognized parking ticket");
    }

    @Test
    void should_throw_WithoutAnyPositionException_when_park_given_super_smart_parking_boy_with_two_parking_lots_without_any_position() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingBoy parkingBoy = new SuperSmartParkingBoy(parkingLot1, parkingLot2);
        Car car1 = new Car("a");
        Car car2 = new Car("a");
        Car car3 = new Car("c");
        //when
        parkingBoy.park(car1);
        parkingBoy.park(car2);
        //then
        WithoutAnyPositionException exceptionMessage = Assertions.assertThrows(WithoutAnyPositionException.class, () -> {
            parkingBoy.park(car3);
        });
        Assertions.assertEquals(exceptionMessage.getMessage(), "No available position");
    }

    @Test
    void should_park_in_the_right_parking_lot_when_park_given_super_smart_parking_boy_with_two_parking_lots_and_two_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(15);
        ParkingBoy parkingBoy = new SuperSmartParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car("a");
        //when
        parkingBoy.park(car);
        //then
        Assertions.assertEquals(parkingLot2, parkingBoy.getParkingLotList().get(1));
    }
}
