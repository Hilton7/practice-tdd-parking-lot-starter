package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLotTest {
    @Test
    void should_return_ticket_when_park_given_parking_lot_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car("a");
        //when
        Ticket ticket = parkingLot.park(car);
        //then
        Assertions.assertEquals(car, ticket.getCar());
    }


    @Test
    void should_return_car_when_fetch_given_parking_lot_and_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car("a");
        //when
        parkingLot.park(new Car("b"));
        Ticket ticket = parkingLot.park(car);
        Car fetchCar = parkingLot.fetch(ticket);
        //then
        Assertions.assertEquals(car, fetchCar);
    }

    @Test
    void should_throw_UnrecognizedTicketException_when_fetch_given_parking_lot_and_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car("a");
        //then
        UnrecognizedTicketException exceptionMessage = Assertions.assertThrows(UnrecognizedTicketException.class, () -> {
            //given
            Ticket ticket = parkingLot.park(car);
            Car fetchCar = parkingLot.fetch(new Ticket());
        });
        Assertions.assertEquals(exceptionMessage.getMessage(), "Unrecognized parking ticket");
    }

    @Test
    void should_throw_UnrecognizedTicketException_when_fetch_given_parking_lot_and_repeat_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car("a");
        //then
        UnrecognizedTicketException exceptionMessage = Assertions.assertThrows(UnrecognizedTicketException.class, () -> {
            //when
            Ticket ticket = parkingLot.park(car);
            Car fetchCar = parkingLot.fetch(ticket);
            fetchCar = parkingLot.fetch(ticket);
        });
        Assertions.assertEquals(exceptionMessage.getMessage(), "Unrecognized parking ticket");
    }

    @Test
    void should_throw_WithoutAnyPositionException_when_park_given_full_parking_lot_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car = new Car("a");
        parkingLot.park(car);
        //then
        WithoutAnyPositionException exceptionMessage = Assertions.assertThrows(WithoutAnyPositionException.class, () -> {
            //when
            parkingLot.park(new Car("b"));
        });
        Assertions.assertEquals(exceptionMessage.getMessage(), "No available position");
    }


    @Test
    void should_throw_WithoutAnyPositionException_when_park_given_parking_lot_without_position_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(0);
        Car car = new Car("a");
        //then
        WithoutAnyPositionException exceptionMessage = Assertions.assertThrows(WithoutAnyPositionException.class, () -> {
            //when
            parkingLot.park(car);
        });
        Assertions.assertEquals(exceptionMessage.getMessage(), "No available position");
    }
}
