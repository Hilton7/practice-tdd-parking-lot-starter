package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLotManagerTest {
    @Test
    void should_return_ticket_when_park_given_parking_lot_manager_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLot);
        Car car = new Car("a");
        //when
        Ticket ticket = parkingLotManager.park(car);
        //then
        Assertions.assertEquals(car, ticket.getCar());
    }


    @Test
    void should_return_car_when_fetch_given_parking_lot_manager_and_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLot);
        Car car = new Car("a");
        //when
        parkingLotManager.park(new Car("b"));
        Ticket ticket = parkingLotManager.park(car);
        Car fetchCar = parkingLotManager.fetch(ticket);
        //then
        Assertions.assertEquals(car, fetchCar);
    }

    @Test
    void should_throw_UnrecognizedTicketException_when_fetch_given_super_parking_lot_manager_and_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLot);
        Car car = new Car("a");
        //then
        UnrecognizedTicketException exceptionMessage = Assertions.assertThrows(UnrecognizedTicketException.class, () -> {
            //given
            Ticket ticket = parkingLotManager.park(car);
            Car fetchCar = parkingLotManager.fetch(new Ticket());
        });
        Assertions.assertEquals(exceptionMessage.getMessage(), "Unrecognized parking ticket");
    }

    @Test
    void should_throw_UnrecognizedTicketException_when_fetch_given_parking_lot_manager_and_repeat_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLot);
        Car car = new Car("a");
        //then
        UnrecognizedTicketException exceptionMessage = Assertions.assertThrows(UnrecognizedTicketException.class, () -> {
            //when
            Ticket ticket = parkingLotManager.park(car);
            Car fetchCar = parkingLotManager.fetch(ticket);
            fetchCar = parkingLotManager.fetch(ticket);
        });
        Assertions.assertEquals(exceptionMessage.getMessage(), "Unrecognized parking ticket");
    }

    @Test
    void should_throw_WithoutAnyPositionException_when_park_given_full_parking_lot_manager_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLot);
        Car car = new Car("a");
        parkingLotManager.park(car);
        //then
        WithoutAnyPositionException exceptionMessage = Assertions.assertThrows(WithoutAnyPositionException.class, () -> {
            //when
            parkingLotManager.park(new Car("b"));
        });
        Assertions.assertEquals(exceptionMessage.getMessage(), "No available position");
    }


    @Test
    void should_throw_WithoutAnyPositionException_when_park_given_super_parking_lot_manager_without_position_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(0);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLot);
        Car car = new Car("a");
        //then
        WithoutAnyPositionException exceptionMessage = Assertions.assertThrows(WithoutAnyPositionException.class, () -> {
            //when
            parkingLotManager.park(car);
        });
        Assertions.assertEquals(exceptionMessage.getMessage(), "No available position");
    }

    @Test
    void should_return_right_car_when_park_given_parking_lot_manager_with_two_parking_lots_and_car_and_two_tickets() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLot1,parkingLot2);
        Car car1 = new Car("a");
        Car car2 = new Car("b");
        //when
        Ticket ticket1 = parkingLotManager.park(car1);
        Ticket ticket2 = parkingLotManager.park(car2);
        Car fetchCar1 = parkingLotManager.fetch(ticket1);
        Car fetchCar2 = parkingLotManager.fetch(ticket2);
        //then
        Assertions.assertEquals(car1, fetchCar1);
        Assertions.assertEquals(car2, fetchCar2);
    }

    @Test
    void should_throw_UnrecognizedException_when_park_given_parking_lot_manager_with_two_parking_lots_and_car_and_unrecognized_tickets() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLot1,parkingLot2);
        Car car1 = new Car("a");
        //when
        parkingLotManager.park(car1);
        //then
        UnrecognizedTicketException exceptionMessage = Assertions.assertThrows(UnrecognizedTicketException.class, () -> {
            parkingLotManager.fetch(new Ticket());
        });
        Assertions.assertEquals(exceptionMessage.getMessage(), "Unrecognized parking ticket");
    }

    @Test
    void should_throw_UnrecognizedException_when_park_given_parking_lot_manager_with_two_parking_lots_and_car_and_repeat_tickets() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLot1,parkingLot2);
        Car car1 = new Car("a");
        //when
        Ticket ticket = parkingLotManager.park(car1);
        parkingLotManager.fetch(ticket);
        //then
        UnrecognizedTicketException exceptionMessage = Assertions.assertThrows(UnrecognizedTicketException.class, () -> {
            parkingLotManager.fetch(ticket);
        });
        Assertions.assertEquals(exceptionMessage.getMessage(), "Unrecognized parking ticket");
    }

    @Test
    void should_throw_WithoutAnyPositionException_when_park_given_parking_lot_manager_with_two_parking_lots_without_any_position() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLot1,parkingLot2);
        Car car1 = new Car("a");
        Car car2 = new Car("a");
        Car car3 = new Car("c");
        //when
        parkingLotManager.park(car1);
        parkingLotManager.park(car2);
        //then
        WithoutAnyPositionException exceptionMessage = Assertions.assertThrows(WithoutAnyPositionException.class, () -> {
            parkingLotManager.park(car3);
        });
        Assertions.assertEquals(exceptionMessage.getMessage(), "No available position");
    }

    @Test
    void should_park_in_the_right_parking_lot_when_park_given_parking_lot_manager_with_two_parking_lots_and_two_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(15);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLot1,parkingLot2);
        Car car = new Car("a");
        //when
        parkingLotManager.park(car);
        //then
        Assertions.assertEquals(parkingLot2, parkingLotManager.getParkingLotList().get(1));
    }

    @Test
    void should_park_in_the_right_parking_lot_when_park_given_parking_lot_manager_with_car_and_parking_lot_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(15);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLot1, parkingLot2);
        parkingLotManager.addParkingLotBoy(new ParkingBoy(parkingLot1, parkingLot2));
        parkingLotManager.addParkingLotBoy(new SmartParkingBoy(parkingLot1,parkingLot2));
        parkingLotManager.addParkingLotBoy(new SuperSmartParkingBoy(parkingLot1,parkingLot2));
        Car car = new Car("a");
        //when
        parkingLotManager.specifyPark(ParkingLotBoyType.PARKING_BOY, car);
        //then
        Assertions.assertEquals(parkingLot1, parkingLotManager.getParkingLotList().get(0));
    }

    @Test
    void should_park_in_the_right_parking_lot_when_park_given_parking_lot_manager_with_car_and_smart_parking_lot_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(15);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLot1, parkingLot2);
        parkingLotManager.addParkingLotBoy(new ParkingBoy(parkingLot1, parkingLot2));
        parkingLotManager.addParkingLotBoy(new SmartParkingBoy(parkingLot1,parkingLot2));
        parkingLotManager.addParkingLotBoy(new SuperSmartParkingBoy(parkingLot1,parkingLot2));
        Car car = new Car("a");
        //when
        parkingLotManager.specifyPark(ParkingLotBoyType.SMART_PARKING_BOY, car);
        //then
        Assertions.assertEquals(parkingLot2, parkingLotManager.getParkingLotList().get(1));
    }

    @Test
    void should_park_in_the_right_parking_lot_when_park_given_parking_lot_manager_with_car_and_super_parking_lot_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(15);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLot1, parkingLot2);
        parkingLotManager.addParkingLotBoy(new SuperSmartParkingBoy(parkingLot1,parkingLot2));
        Car car = new Car("a");
        //when
        parkingLotManager.specifyPark(ParkingLotBoyType.SUPER_SMART_PARKING_BOY, car);
        parkingLotManager.specifyPark(ParkingLotBoyType.SUPER_SMART_PARKING_BOY, new Car("2"));
        //then
        Assertions.assertEquals(parkingLot2, parkingLotManager.getParkingLotList().get(1));
    }


    @Test
    void should_return_right_car_when_park_given_parking_lot_manager_with_car_and_super_parking_lot_boy_and_smart_parking_lot_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(15);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLot1, parkingLot2);
        parkingLotManager.addParkingLotBoy(new ParkingBoy(parkingLot1, parkingLot2));
        parkingLotManager.addParkingLotBoy(new SmartParkingBoy(parkingLot1,parkingLot2));
        parkingLotManager.addParkingLotBoy(new SuperSmartParkingBoy(parkingLot1,parkingLot2));
        Car car = new Car("a");
        //when
        Ticket ticket = parkingLotManager.specifyPark(ParkingLotBoyType.SMART_PARKING_BOY, car);
        Car fetchCar = parkingLotManager.specifyFetch(ParkingLotBoyType.SUPER_SMART_PARKING_BOY, ticket);
        //then
        Assertions.assertEquals(car, fetchCar);
    }

    @Test
    void should_throw_WithoutAnyPositionException_when_park_given_parking_lot_manager_with_two_parking_lots_without_any_position_and_smart_parking_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLot1, parkingLot2);
        parkingLotManager.addParkingLotBoy(new SmartParkingBoy(parkingLot1,parkingLot2));
        Car car1 = new Car("a");
        Car car2 = new Car("a");
        Car car3 = new Car("c");
        //when
        parkingLotManager.park(car1);
        parkingLotManager.specifyPark(ParkingLotBoyType.SMART_PARKING_BOY,car2);
        //then
        WithoutAnyPositionException exceptionMessage = Assertions.assertThrows(WithoutAnyPositionException.class, () -> {
            parkingLotManager.specifyPark(ParkingLotBoyType.SMART_PARKING_BOY,car3);
        });
        Assertions.assertEquals(exceptionMessage.getMessage(), "No available position");
    }


    @Test
    void should_throw_UnrecognizedException_when_park_given_parking_lot_manager_with_two_parking_lots_and_car_and_repeat_tickets_and_super_smart_parking_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLot1, parkingLot2);
        parkingLotManager.addParkingLotBoy(new SuperSmartParkingBoy(parkingLot1, parkingLot2));
        Car car1 = new Car("a");
        //when
        Ticket ticket = parkingLotManager.specifyPark(ParkingLotBoyType.SUPER_SMART_PARKING_BOY,car1);
        parkingLotManager.specifyFetch(ParkingLotBoyType.SUPER_SMART_PARKING_BOY,ticket);
        //then
        UnrecognizedTicketException exceptionMessage = Assertions.assertThrows(UnrecognizedTicketException.class, () -> {
            parkingLotManager.specifyFetch(ParkingLotBoyType.SUPER_SMART_PARKING_BOY,ticket);
        });
        Assertions.assertEquals(exceptionMessage.getMessage(), "Unrecognized parking ticket");
    }


    @Test
    void should_throw_UnrecognizedException_when_park_given_parking_lot_manager_with_two_parking_lots_and_car_and_unrecognized_tickets_and_super_smart_parking_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLot1, parkingLot2);
        parkingLotManager.addParkingLotBoy(new SuperSmartParkingBoy(parkingLot1, parkingLot2));
        Car car1 = new Car("a");
        //when
        parkingLotManager.specifyPark(ParkingLotBoyType.SUPER_SMART_PARKING_BOY,car1);
        //then
        UnrecognizedTicketException exceptionMessage = Assertions.assertThrows(UnrecognizedTicketException.class, () -> {
            parkingLotManager.specifyFetch(ParkingLotBoyType.SUPER_SMART_PARKING_BOY,new Ticket());
        });
        Assertions.assertEquals(exceptionMessage.getMessage(), "Unrecognized parking ticket");
    }
}


