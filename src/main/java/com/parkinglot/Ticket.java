package com.parkinglot;

import java.util.Objects;
import java.util.UUID;

public class Ticket {
    private String uuid;
    private Car car;

    public Ticket(Car car) {
        this.uuid = UUID.randomUUID().toString().substring(0, 5);
        this.car = car;
    }

    public Car getCar() {
        return car;
    }

    @Override
    public boolean equals(Object obj) {
        if (Objects.isNull(obj) || Objects.isNull(car) || !(obj instanceof Ticket))
            return false;
        Ticket ticketObj = (Ticket) obj;
        return this.uuid.equals(ticketObj.getUUid());
    }

    public String getUUid() {
        return uuid;
    }

    public Ticket() {
        this.uuid = UUID.randomUUID().toString().substring(0, 5);
    }
}
