package com.parkinglot;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ParkingLot {
    private Integer capacity = 10;
    private Integer totalCapacity = 10;
    private Map<String, Ticket> tickets;

    public Ticket park(Car car) {
        if (isFull())
            throw new WithoutAnyPositionException("No available position");
        Ticket ticket = new Ticket(car);
        this.tickets.put(ticket.getUUid(), ticket);
        this.capacity--;
        return ticket;
    }

    public Car fetch(Ticket ticket) {
        if (Objects.isNull(ticket) || !this.tickets.containsKey(ticket.getUUid()))
            throw new UnrecognizedTicketException("Unrecognized parking ticket");
        Ticket fetchTicket = this.tickets.remove(ticket.getUUid());
        this.capacity++;
        return fetchTicket.getCar();
    }

    public ParkingLot(Integer capacity) {
        this.totalCapacity = this.capacity = capacity;
        this.tickets = new HashMap<>();
    }

    public ParkingLot() {
        this.tickets = new HashMap<>();
    }

    private boolean isFull() {
        return this.capacity == 0;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public Integer getTotalCapacity() {
        return totalCapacity;
    }
}
