package com.parkinglot;

import java.util.Objects;

public class Car {
    private String carId;

    public Car(String carId) {
        this.carId = carId;
    }


    public String getCarId() {
        return carId;
    }

    @Override
    public boolean equals(Object obj) {
        if (Objects.isNull(obj) || !(obj instanceof Car))
            return false;
        Car carObj = (Car) obj;
        return carObj.getCarId().equals(this.carId);
    }
}
