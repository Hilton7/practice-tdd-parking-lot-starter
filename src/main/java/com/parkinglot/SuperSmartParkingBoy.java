package com.parkinglot;

import java.util.Comparator;
import java.util.Optional;

public class SuperSmartParkingBoy extends ParkingBoy {
    public SuperSmartParkingBoy() {

    }

    public SuperSmartParkingBoy(ParkingLot... parkingLots) {
        super(parkingLots);
    }

    @Override
    public Ticket park(Car car) {
        Optional<ParkingLot> maxCapacityParkingLot = this.parkingLotList.stream()
                .max(Comparator.comparingInt(parkingLot -> (parkingLot.getCapacity() / parkingLot.getTotalCapacity())));
        return maxCapacityParkingLot.get().park(car);
    }
}
