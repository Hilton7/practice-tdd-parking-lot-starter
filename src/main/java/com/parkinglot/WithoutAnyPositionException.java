package com.parkinglot;

public class WithoutAnyPositionException extends RuntimeException {
    public WithoutAnyPositionException(String message) {
        super(message);
    }
}
