package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class ParkingBoy {
    protected List<ParkingLot> parkingLotList;

    public ParkingBoy(ParkingLot... parkingLots) {
        this.parkingLotList = new ArrayList<>();
        for (int i = 0; i < parkingLots.length; i++) {
            this.parkingLotList.add(parkingLots[i]);
        }
    }

    public Ticket park(Car car) {
        Ticket ticket;
        for (int i = 0; i < parkingLotList.size(); i++) {
            try {
                return parkingLotList.get(i).park(car);
            } catch (WithoutAnyPositionException e) {
                if (i == parkingLotList.size() - 1)
                    throw e;
            }
        }
        return null;
    }

    public Car fetch(Ticket ticket) {
        for (int i = 0; i < parkingLotList.size(); i++) {
            try {
                return parkingLotList.get(i).fetch(ticket);
            } catch (UnrecognizedTicketException e) {
                if (i == parkingLotList.size() - 1)
                    throw e;
            }
        }
        return null;
    }

    public List<ParkingLot> getParkingLotList() {
        return parkingLotList;
    }
}
