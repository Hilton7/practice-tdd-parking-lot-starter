package com.parkinglot;

import java.util.HashMap;
import java.util.Map;

public class ParkingLotManager extends ParkingBoy {
    private Map<ParkingLotBoyType,ParkingBoy> parkingBoys;

    public ParkingLotManager(ParkingLot... parkingLots) {
        super(parkingLots);
        this.parkingBoys = new HashMap<>();
    }

    public void addParkingLotBoy(ParkingBoy parkingBoy) {
        ParkingLotBoyType parkingLotType = ParkingLotBoyType.PARKING_BOY;
        if(parkingBoy instanceof SuperSmartParkingBoy) {
            parkingLotType = ParkingLotBoyType.SUPER_SMART_PARKING_BOY;
        }else if(parkingBoy instanceof SmartParkingBoy) {
            parkingLotType = ParkingLotBoyType.SMART_PARKING_BOY;
        }
        parkingBoys.put(parkingLotType,parkingBoy);
    }

    public Car specifyFetch(ParkingLotBoyType parkingLotBoyType,Ticket ticket) {
        return parkingBoys.get(parkingLotBoyType).fetch(ticket);
    }

    public Ticket specifyPark(ParkingLotBoyType parkingLotBoyType,Car car) {
        return parkingBoys.get(parkingLotBoyType).park(car);
    }
}
