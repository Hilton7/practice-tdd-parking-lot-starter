package com.parkinglot;

import java.util.Comparator;
import java.util.Optional;

public class SmartParkingBoy extends ParkingBoy {
    public SmartParkingBoy(ParkingLot ...parkingLots) {
        super(parkingLots);
    }

    public SmartParkingBoy() {
    }

    @Override
    public Ticket park(Car car) {
        Optional<ParkingLot> maxCapacityParkingLot = this.parkingLotList.stream().max(Comparator.comparingInt(ParkingLot::getCapacity));
        return maxCapacityParkingLot.get().park(car);
    }
}
